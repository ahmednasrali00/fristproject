import 'package:sheraa/ui/bottom_navigation/bottom_navigation.dart';
import 'package:sheraa/ui/choose_country/choose_country.dart';
import 'package:sheraa/ui/gender/select_gender.dart';
import 'package:sheraa/ui/language/language_screen.dart';
import 'package:sheraa/ui/splash/splash_screen.dart';

final routes = {
  '/': (context) => SplashScreen(),
  '/choose_language': (context) => ChooseLanguageScreen(),
  '/choose_country': (context) => ChooseCountryScreen(),
  '/select_gender': (context) => SelectGenderScreen(),
  '/bottom_navigation': (context) => BottomNavigation()
};
