class Urls {
  static const String BASE_URL = "https://pm-matrix.com/api";
  static const String LOGIN_URL = BASE_URL + "/login";
  static const String USER_REGISTER_URL = BASE_URL + "/register";
  static const String ORGANIZATION_REGISTER_URL =
      BASE_URL + "/register_provider";
  static const String ACTIVATION_CODE_URL = BASE_URL + "/verification/verify";
  static const String RESEND_ACTIVE_CODE_URL = BASE_URL + "/verification/send";
  static const String FORGET_PASSWORD_URL = BASE_URL + "/password/forget";
  static const String CONFIRM_CODE_URL = BASE_URL + "/password/code";
  static const String RESET_PASSWORD_URL = BASE_URL + "/password/reset";
  static const String SLIDER_URL = BASE_URL + "/sliders";
  static const String CONTRACT_URL = BASE_URL + "/user/contract";
  static const String CONTRACTS_URL = BASE_URL + "/user/contracts";
  static const String MAIN_AREAS_URL = BASE_URL + "/cities";
  static const String SUB_AREAS_URL = BASE_URL + "/areas";
  static const String SERVICES_URL = BASE_URL + "/services";
  static const String SERVICES_TYPE_URL = BASE_URL + "/service_types";
  static const String DEVICES_URL = BASE_URL + "/devices";
  static const String ORDER_PERIOD_URL = BASE_URL + "/available_hours";
  static const String MEDIAS_URL = BASE_URL + "/uploader/media/upload";
  static const String REQUEST_ORDER_URL = BASE_URL + "/orders";

  static const String UPDATE_PROFILE_URL = BASE_URL + "/profile";
  static const String ORDERS_URL = BASE_URL + "/orders";
  static const String ORDER_DETAILS_URL = BASE_URL + "/orders/";
  static const String CANCEL_ORDER__URL = BASE_URL + "/orders/cancel";
  static const String RATING_ORDER__URL = BASE_URL + "/ratings/";
  static const String REPORTS_URL = BASE_URL + "/customer/reports";
  static const String REPORT_DETAILS_URL = BASE_URL + "/reports/";
  static const String ACCEPT_REPORT = BASE_URL + "/orders/";
  static const String REFUSE_REPORT = BASE_URL + "/orders/";
  static const String REQUEST_FIX_STEPS_URL = BASE_URL + "/steps";
  static const String CANTACT_US_URL = BASE_URL + "/feedback";
  static const String PRIVACY_URL = BASE_URL + "/policy";
  static const String ABOUT_US_URL = BASE_URL + "/about";
  static const String SOCIAL_URL = BASE_URL + "/settings";
  static const String NOTIFICATION_URL = BASE_URL + "/notifications";
  static const String NOTIFICATION_UNREAD_URL =
      BASE_URL + "/notifications/unread";
  static const String NOTIFICATION_DELETED_ALL_URL =
      BASE_URL + "/notifications/deleteAll";
  static const String NOTIFICATION_DELETED_SINGLE_UR =
      BASE_URL + "/notifications/";

  static const String LOGOUT_URL = BASE_URL + "/user/logout";
  static bool testPrice = true;
}
