

// class NotificationItem extends StatelessWidget {
//   final AnimationController animationController;
//   final Animation animation;
//   final NotificationModel notificationModel;

//   const NotificationItem(
//       {Key key,
//       this.animationController,
//       this.animation,
//       this.notificationModel})
//       : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     double _width = MediaQuery.of(context).size.width;
//     double _height = MediaQuery.of(context).size.width;

//     return AnimatedBuilder(
//         animation: animationController,
//         builder: (context, Widget child) {
//           return FadeTransition(
//             opacity: animation,
//             child: Transform(
//               transform: new Matrix4.translationValues(
//                   0.0, 50 * (1.0 - animation.value), 0.0),
//               child: Dismissible(
//                 direction: DismissDirection.startToEnd,
//                 key: ValueKey(
//                   'notificationModel.id',
//                 ),
//                 background: Container(
//                   alignment: Alignment.centerRight,
//                   //margin: EdgeInsets.symmetric(),
//                   padding: EdgeInsets.only(right: 40),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       Image.asset(
//                         "assets/images/delnotifi.png",
//                         height: 40,
//                         width: 40,
//                       ),
//                       Text(
//                         'حذف',
//                         style: TextStyle(
//                             color: mainAppColor,
//                             fontSize: 15,
//                             fontWeight: FontWeight.w500),
//                       )
//                     ],
//                   ),
//                 ),
//                 confirmDismiss: (direction) {
//                   return showDialog(
//                     context: context,
//                     builder: (ctx) {
//                       // return ConfirmationDialog(
//                       //     alertMessage: "هل تريد مسح هذا الاشعار ؟",
//                       //     onPressedConfirm: () async {
//                       //       // await Provider.of<SettingProvider>(context, listen: false)
//                       //       //     .removeNotifcation(context, _notificationItem.id)
//                       //       //     .then((value) {
//                       //       //   if (value) {
//                       //       //     Provider.of<SettingProvider>(context, listen: false)
//                       //       //         .deletenotifcationlocally(_notificationItem.id);

//                       //       //     Navigator.pop(context);
//                       //       //     Commons.showToast(
//                       //       //         message: "تم مسح الاشعار بنجاح", context: context);
//                       //       //   }
//                       //       // });
//                       return AlertDialog(
//                         contentPadding: EdgeInsets.only(top: 10),
//                         shape: RoundedRectangleBorder(
//                             borderRadius:
//                                 BorderRadius.all(Radius.circular(10.0))),
//                         content: Container(
//                           height: _width * .6,
//                           width: _width * 0.7,
//                           child: SingleChildScrollView(
//                             child: Column(
//                               mainAxisAlignment: MainAxisAlignment.start,
//                               children: [
//                                 Container(
//                                   // height: 80,
//                                   // width: 80,
//                                   child: Image.asset(
//                                     'assets/images/cancel.png',
//                                     fit: BoxFit.scaleDown,
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: 10,
//                                 ),
//                                 Text(
//                                   "مسح الاشعار",
//                                   textAlign: TextAlign.center,
//                                   style: TextStyle(
//                                       fontFamily: 'Expo',
//                                       fontSize: 18,
//                                       color: blackColor,
//                                       fontWeight: FontWeight.w500),
//                                 ),
//                                 SizedBox(
//                                   height: 10,
//                                 ),
//                                 Text('هل تريد مسح هذا الاشعار ؟',
//                                     textAlign: TextAlign.center,
//                                     style: TextStyle(
//                                         fontFamily: 'Expo',
//                                         fontSize: 15,
//                                         color: textGrayColor,
//                                         fontWeight: FontWeight.w500)),
//                                 SizedBox(
//                                   height: 10,
//                                 ),
//                                 Row(
//                                   mainAxisAlignment: MainAxisAlignment.start,
//                                   children: [
//                                     Container(
//                                       width: _width * .35,
//                                       margin: EdgeInsets.symmetric(
//                                           horizontal: _width * 0.02),
//                                       child: CustomButton(
//                                           enableHorizontalMargin: false,
//                                           btnLbl: "نعم",
//                                           btnStyle: TextStyle(
//                                               fontSize: 15, color: whiteColor),
//                                           height: 50,
//                                           onPressedFunction: () async {
//                                             await Provider.of<
//                                                         NotificationsProvider>(
//                                                     context,
//                                                     listen: false)
//                                                 .deleteSingleNotification(
//                                                     context,
//                                                     notificationModel.id);
//                                           }),
//                                     ),
//                                     Container(
//                                       width: _width * 0.35,
//                                       //  margin: EdgeInsets.symmetric(horizontal: _width * 0.02),
//                                       child: CustomButton(
//                                           enableHorizontalMargin: false,
//                                           btnLbl: "لا",
//                                           btnColor: Colors.white,
//                                           borderColor: mainAppColor,
//                                           btnStyle: TextStyle(
//                                               color: mainAppColor,
//                                               fontWeight: FontWeight.w400,
//                                               fontSize: 16.0),
//                                           onPressedFunction: () =>
//                                               Navigator.pop(context)),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       );
//                     },
//                   );
//                 },
//                 onDismissed: (direction) {},
//                 child: Container(
//                   margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
//                   decoration: BoxDecoration(
//                       borderRadius: BorderRadius.all(Radius.circular(10)),
//                       color: cardColor),
//                   child: ListTile(
//                     leading: Image.asset(
//                       "assets/images/bell.png",
//                       // height: 40,
//                       // width: 40,
//                     ),
//                     title: Text(
//                       // 'فاتورة لطلب رقم #5964545 واردة اليك اطلع على تفاصيلها الان',
//                       notificationModel.body,
//                       style: TextStyle(
//                           fontWeight: FontWeight.w500,
//                           fontSize: 14,
//                           color: Color(0xff1C1819)),
//                     ),
//                     subtitle: Text(
//                         // 'منذ دقيقتين'
//                         notificationModel.createdAtFormated),
//                   ),
//                 ),
//               ),
//             ),
//           );
//         });
//   }
// }
