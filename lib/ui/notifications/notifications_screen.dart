import 'package:flutter/material.dart';
import 'package:sheraa/custom_widgets/connectivity/network_indicator.dart';
import 'package:sheraa/custom_widgets/safe_area/page_container.dart';

class NotificationsScreen extends StatefulWidget {
  const NotificationsScreen({Key? key}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen>
    with TickerProviderStateMixin {
  double _width = 0, _height = 0;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  // AnimationController _animationController;
  // NotificationsProvider _notificationsProvider;
  // Future<List<NotificationModel>> _notificationsList;

  @override
  void initState() {
    //  _animationController = AnimationController(
    //    duration: Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  bool initialRun = true;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (initialRun) {
      // _notificationsProvider = Provider.of<NotificationsProvider>(context);
      // _notificationsList = _notificationsProvider.getNotificationList(context);
      initialRun = false;
    }
  }

  @override
  void dispose() {
    // _animationController.dispose();
    super.dispose();
  }

  // Widget _buildBody() {
  //   return FutureBuilder<List<NotificationModel>>(
  //       future: _notificationsList,
  //       builder: (context, snapshot) {
  //         switch (snapshot.connectionState) {
  //           case ConnectionState.none:
  //             return Center(
  //               child: SpinKitPulse(color: mainAppColor),
  //             );
  //           case ConnectionState.active:
  //             return Text('');
  //           case ConnectionState.waiting:
  //             return Center(
  //               child: SpinKitFadingCircle(color: mainAppColor),
  //             );
  //           case ConnectionState.done:
  //             if (snapshot.hasError)
  //               return Error(
  //                   errorMessage:
  //                       AppLocalizations.of(context).translate('error'));
  //             else {
  //               if (snapshot.data.length > 0)
  //                 return ListView.builder(
  //                     shrinkWrap: true,
  //                     padding: const EdgeInsets.all(10),
  //                     itemCount: snapshot.data.length,
  //                     itemBuilder: (ctx, i) {
  //                       var animation = Tween(begin: 0.0, end: 1.0).animate(
  //                           CurvedAnimation(
  //                               parent: _animationController,
  //                               curve: Interval((1 / 10 * i), 1.0,
  //                                   curve: Curves.fastOutSlowIn)));
  //                       _animationController.forward();
  //                       return NotificationItem(
  //                         animation: animation,
  //                         animationController: _animationController,
  //                         notificationModel: snapshot.data[i],
  //                       );
  //                     });
  //               else
  //                 return Center(
  //                   child: NoData(
  //                       message: AppLocalizations.of(context)
  //                           .translate('no_results')),
  //                 );
  //             }
  //         }
  //         return Center(
  //           child: SpinKitFadingCircle(color: mainAppColor),
  //         );
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return NetworkIndicator(
      child: PageContainer(
        child: Scaffold(
          //backgroundColor: whiteColor,
          key: _scaffoldKey,
          //drawer: DrawerMenu(),
          // appBar: CustomAppbar(
          //   hasBackBtn: false,
          //   hasnotification: false,
          //   hasCenterLogo: false,
          //   title: 'الإشعارات',
          //   leadingWidget: IconButton(
          //       icon: Image(
          //         image: AssetImage(
          //           'assets/images/menu.png',
          //         ),
          //       ),
          //       onPressed: () => _scaffoldKey.currentState.openDrawer()),
          //   trailingWidget: IconButton(
          //       icon: Image(
          //         image: AssetImage(
          //           'assets/images/deletenotifi.png',
          //         ),
          //       ),
          //       onPressed: () {
          //         return showDialog(
          //             context: context,
          //             builder: (ctx) {
          //               return AlertDialog(
          //                 contentPadding: EdgeInsets.only(top: 10),
          //                 shape: RoundedRectangleBorder(
          //                     borderRadius:
          //                         BorderRadius.all(Radius.circular(10.0))),
          //                 content: Container(
          //                   height: _width * .6,
          //                   width: _width * 0.7,
          //                   child: SingleChildScrollView(
          //                     child: Column(
          //                       mainAxisAlignment: MainAxisAlignment.start,
          //                       children: [
          //                         Container(
          //                           // height: 80,
          //                           // width: 80,
          //                           child: Image.asset(
          //                             'assets/images/cancel.png',
          //                             fit: BoxFit.scaleDown,
          //                           ),
          //                         ),
          //                         SizedBox(
          //                           height: 10,
          //                         ),
          //                         Text(
          //                           "مسح الاشعارات",
          //                           textAlign: TextAlign.center,
          //                           style: TextStyle(
          //                               fontFamily: 'Expo',
          //                               fontSize: 18,
          //                               color: blackColor,
          //                               fontWeight: FontWeight.w500),
          //                         ),
          //                         SizedBox(
          //                           height: 10,
          //                         ),
          //                         Text('هل تريد مسح الاشعارات ؟',
          //                             textAlign: TextAlign.center,
          //                             style: TextStyle(
          //                                 fontFamily: 'Expo',
          //                                 fontSize: 15,
          //                                 color: textGrayColor,
          //                                 fontWeight: FontWeight.w500)),
          //                         SizedBox(
          //                           height: 10,
          //                         ),
          //                         Row(
          //                           mainAxisAlignment: MainAxisAlignment.start,
          //                           children: [
          //                             Container(
          //                               width: _width * .35,
          //                               margin: EdgeInsets.symmetric(
          //                                   horizontal: _width * 0.02),
          //                               child: CustomButton(
          //                                   enableHorizontalMargin: false,
          //                                   btnLbl: "نعم",
          //                                   btnStyle: TextStyle(
          //                                       fontSize: 15,
          //                                       color: whiteColor),
          //                                   height: 50,
          //                                   onPressedFunction: () async {
          //                                     await Provider.of<
          //                                                 NotificationsProvider>(
          //                                             context,
          //                                             listen: false)
          //                                         .deletedAllNotifications(
          //                                             context);
          //                                   }),
          //                             ),
          //                             Container(
          //                               width: _width * 0.35,
          //                               //  margin: EdgeInsets.symmetric(horizontal: _width * 0.02),
          //                               child: CustomButton(
          //                                   enableHorizontalMargin: false,
          //                                   btnLbl: "لا",
          //                                   btnColor: Colors.white,
          //                                   borderColor: mainAppColor,
          //                                   btnStyle: TextStyle(
          //                                       color: mainAppColor,
          //                                       fontWeight: FontWeight.w400,
          //                                       fontSize: 16.0),
          //                                   onPressedFunction: () =>
          //                                       Navigator.pop(context)),
          //                             ),
          //                           ],
          //                         ),
          //                       ],
          //                     ),
          //                   ),
          //                 ),
          //               );
          //             });
          //       }),
          // ),
          // body: _buildBody(),
        ),
      ),
    );
  }
}
