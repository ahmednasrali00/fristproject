import 'package:flutter/material.dart';
import 'package:sheraa/custom_widgets/safe_area/page_container.dart';
import 'package:sheraa/shared_preferences/shared_preferences_helper.dart';
import 'package:sheraa/utils/app_colors.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:percent_indicator/percent_indicator.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
 //from ahmed version 2
}

class _SplashScreenState extends State<SplashScreen> {
  double _height = 0, _width = 0;
  //AuthProvider _authProvider;

  bool _initalRun = true;

  Future initData() async {
    await Future.delayed(Duration(seconds: 2));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_initalRun) {
      // _authProvider = Provider.of<AuthProvider>(context);
      // _getLanguage();
      // initData().then((value) async {
      //   //   await _checkIsLogin();
      //   Navigator.pushNamed(context, '/choose_language');
      // });

      _initalRun = false;
    }
  }

  // Future<Null> _checkIsLogin() async {
  //   var _user = await SharedPreferencesHelper.read("user");
  //   String token = await SharedPreferencesHelper.getUsersetUserToken();
  //   if (_user != null) {
  //     _authProvider.setCurrentUser(User.fromJson(_user));
  //     _authProvider.setUserToken(token);
  //     Navigator.pushReplacementNamed(context, '/home_screen');
  //   } else {
  //     Navigator.pushReplacementNamed(context, '/login_screen');
  //     print('splash');
  //   }
  // }

  Future<void> _getLanguage() async {
    String currentLang = await SharedPreferencesHelper.getUserLang();

    // _authProvider.setCurrentLanguage(currentLang);
    print('current lang: ' + currentLang);
    // print('lang' + _authProvider.currentLang);
  }

  @override
  void initState() {
    super.initState();
    _getLanguage();
    // initData().then((value) {
    //   //   _checkIsLogin();
    // });
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return PageContainer(
        child: Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/logo.png',
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: new LinearPercentIndicator(
                width: MediaQuery.of(context).size.width - 50,
                animation: true,
                lineHeight: 12.0,
                animationDuration: 2000,
                percent: 1,
                progressColor: mainAppColor,
                backgroundColor: secondryMainColor,
                isRTL: true,
                barRadius: Radius.circular(15),
                onAnimationEnd: () {
                  Navigator.pushNamed(context, '/choose_language');
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
