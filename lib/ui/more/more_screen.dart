import 'package:flutter/material.dart';
import 'package:sheraa/custom_widgets/buttons/custom_button.dart';
import 'package:sheraa/custom_widgets/dialogs/login_dialog.dart';
import 'package:sheraa/custom_widgets/dialogs/login_dialog.dart';
import 'package:sheraa/locale/app_localizations.dart';
import 'package:sheraa/ui/more/widgets/more_item.dart';
import 'package:sheraa/utils/app_colors.dart';

class MoreScreen extends StatefulWidget {
  const MoreScreen({Key? key}) : super(key: key);

  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  double _height = 0, _width = 0;
  bool _isclicked = false;
  Widget _buildBody() {
    return Container(
      color: whiteColor,
      child: ListView(
        children: [
          ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 20),
            leading: Image.asset('assets/images/user.png'),
            title: Text(
              AppLocalizations.of(context)!.translate('hello'),
              style: TextStyle(color: blackColor, fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              AppLocalizations.of(context)!.translate('ad_destination'),
              style: TextStyle(
                  color: secondGrayColor,
                  fontWeight: FontWeight.w200,
                  fontSize: 14),
            ),
            trailing: Image.asset('assets/images/arrow_simple.png'),
            onTap: () {
              print('tapppped');
            },
          ),
          SizedBox(
            height: 20,
          ),
          Divider(
            thickness: 8,
            color: accentColor,
          ),
          CustomButton(
              btnLbl:
                  AppLocalizations.of(context)!.translate('login_or_singup'),
              onPressedFunction: () {
                Navigator.of(context).push(PageRouteBuilder(
                    opaque: false,
                    pageBuilder: (BuildContext context, _, __) =>
                        LoginDialog()));
              },
              btnColor: whiteColor,
              enableHorizontalMargin: true,
              btnStyle:
                  TextStyle(fontWeight: FontWeight.bold, color: mainAppColor),
              borderColor: mainAppColor),
          Container(
            height: _height * 0.04,
            margin: EdgeInsets.symmetric(horizontal: _width * 0.04),
            decoration: BoxDecoration(
                color: accentColor,
                borderRadius: BorderRadius.all(Radius.circular(12))),
            alignment: Alignment.center,
            child: Text(
              AppLocalizations.of(context)!.translate('login_des'),
              textAlign: TextAlign.center,
              style: TextStyle(color: textGrayColor, fontSize: 14),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Divider(
            thickness: 8,
            color: accentColor,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                child: Column(
                  children: [
                    Image.asset('assets/images/wallet.png'),
                    Text(AppLocalizations.of(context)!.translate('wallet'))
                  ],
                ),
              ),
              InkWell(
                  child: Column(
                children: [
                  Image.asset('assets/images/star.png'),
                  Text(AppLocalizations.of(context)!.translate('earned_points'))
                ],
              )),
              InkWell(
                  child: Column(
                children: [
                  Image.asset('assets/images/mouse.png'),
                  Text(AppLocalizations.of(context)!.translate('my_movements'))
                ],
              ))
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            thickness: 8,
            color: accentColor,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset('assets/images/subtitle.png'),
                    Text(AppLocalizations.of(context)!.translate('my_ads'))
                  ],
                ),
              ),
              InkWell(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset('assets/images/lovely.png'),
                  Text(
                      AppLocalizations.of(context)!.translate('myfavorite_ads'))
                ],
              )),
              InkWell(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset('assets/images/search-status.png'),
                  Text(AppLocalizations.of(context)!.translate('ad_requests'))
                ],
              ))
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            thickness: 8,
            color: accentColor,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset('assets/images/ranking.png'),
                    Text(AppLocalizations.of(context)!
                        .translate('account_upgrade'))
                  ],
                ),
              ),
              InkWell(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset('assets/images/crown.png'),
                  Text(AppLocalizations.of(context)!.translate('ad_pormotion'))
                ],
              )),
              InkWell(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset('assets/images/page.png'),
                  Text(AppLocalizations.of(context)!
                      .translate('create_your_page'))
                ],
              ))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Divider(
            thickness: 8,
            color: accentColor,
          ),
          MoreItem(
            imgUrl: 'assets/images/people.png',
            title: 'أنواع الحسابات (العضويات)',
            hastrailingImage: false,
            hastrailingTilte: false,
          ),
          Divider(
            color: accentColor,
            thickness: 2,
          ),
          MoreItem(
            imgUrl: 'assets/images/star.png',
            title: 'نظام النقاط المكتسبة',
            hastrailingImage: false,
            hastrailingTilte: false,
          ),
          Divider(
            thickness: 2,
            color: accentColor,
          ),
          MoreItem(
            imgUrl: 'assets/images/global.png',
            title: 'الدولة',
            hastrailingImage: true,
            trailingImageUrl: 'assets/images/saudi-arabia.png',
            hastrailingTilte: false,
          ),
          Divider(
            thickness: 3,
            color: accentColor,
            indent: 15.0,
            endIndent: 10,
          ),
          MoreItem(
            imgUrl: 'assets/images/global.png',
            title: 'المدينة',
            hastrailingImage: false,
            hastrailingTilte: true,
            trailingTitle: 'الرياض',
          ),
          Divider(
            thickness: 3,
            color: accentColor,
            indent: 15.0,
            endIndent: 10,
          ),
          MoreItem(
            imgUrl: 'assets/images/moon.png',
            title: 'الوضع الليلي',
            hastrailingImage: false,
            hastrailingTilte: false,
          ),
          Divider(
            thickness: 3,
            color: accentColor,
            indent: 15.0,
            endIndent: 10,
          ),
          MoreItem(
            imgUrl: 'assets/images/calling.png',
            title: 'تواصل معنا',
            hastrailingImage: false,
            hastrailingTilte: false,
          ),
          Divider(
            thickness: 3,
            color: accentColor,
            indent: 15.0,
            endIndent: 10,
          ),
          MoreItem(
            imgUrl: 'assets/images/user-add.png',
            title: 'انضم الى فريقنا',
            hastrailingImage: false,
            hastrailingTilte: false,
          ),
          Container(
            // height: _height * 0.04,
            // margin: EdgeInsets.symmetric(horizontal: _width * 0.04),
            decoration: BoxDecoration(
              color: accentColor,
              //borderRadius: BorderRadius.all(Radius.circular(12))
            ),
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      child: Text(AppLocalizations.of(context)!
                          .translate('rules_and_terms')),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    InkWell(
                      child: Text(AppLocalizations.of(context)!
                          .translate('privacy_and_policy')),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  AppLocalizations.of(context)!.translate('app_version') +
                      " " +
                      "3.0.1",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: textGrayColor, fontSize: 14),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      // backgroundColor: Color(0xffEFEFEF),
      backgroundColor: _isclicked ? Colors.black.withOpacity(0.2) : whiteColor,
      body: _buildBody(),
    );
  }
}
