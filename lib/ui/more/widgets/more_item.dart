import 'package:flutter/material.dart';
import 'package:sheraa/utils/app_colors.dart';

class MoreItem extends StatefulWidget {
  final String? imgUrl;
  final String? title;
  final bool? setColor;
  final bool? hastrailingImage;
  final bool? hastrailingTilte;
  final String? trailingImageUrl;
  final String? trailingTitle;
  final String? route;
  const MoreItem(
      {Key? key,
      this.imgUrl,
      this.title,
      this.setColor,
      this.route,
      this.hastrailingImage,
      this.hastrailingTilte,
      this.trailingImageUrl,
      this.trailingTitle})
      : super(key: key);

  @override
  _MoreItemState createState() => _MoreItemState();
}

class _MoreItemState extends State<MoreItem> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return Container(
      height: 40,
      child: InkWell(
        onTap: () {
          //   if (widget.route == '/navigation')
          //     provider.upadateNavigationIndex(2);

          //  else if (widget.title == 'تسجيل الخروج')
          //    showDialog(
          //     barrierDismissible: true,
          //     context: context,
          //     builder: (_) {
          //       return LogoutDialog(
          //       );
          //     });
          //  else
          Navigator.pushNamed(context, widget.route!);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: width * 0.05, vertical: 8),
                  child: Image.asset(
                    widget.imgUrl!,
                    color: mainAppColor,
                  ),
                ),
                Text(
                  widget.title!,
                  style: TextStyle(color: Colors.black, fontSize: 14),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                widget.hastrailingImage!
                    ? Container(
                        margin: EdgeInsets.symmetric(
                            // horizontal: width * 0.05,
                            vertical: 8),
                        child: Image.asset(
                          widget.trailingImageUrl!,
                          // color: mainAppColor,
                        ),
                      )
                    : widget.hastrailingTilte!
                        ? Text(
                            widget.trailingTitle!,
                            style: TextStyle(color: Colors.black, fontSize: 14),
                          )
                        : Container(),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: width * 0.03),
                  child: Image.asset(
                    'assets/images/arrow_simple.png',
                    color: secondGrayColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
