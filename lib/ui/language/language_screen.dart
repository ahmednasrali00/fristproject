import 'package:flutter/material.dart';
import 'package:sheraa/custom_widgets/custom_container/custom_container.dart';
import 'package:sheraa/locale/app_localizations.dart';

class ChooseLanguageScreen extends StatefulWidget {
  const ChooseLanguageScreen({Key? key}) : super(key: key);

  @override
  _ChooseLanguageScreenState createState() => _ChooseLanguageScreenState();
}

class _ChooseLanguageScreenState extends State<ChooseLanguageScreen> {
  double? _height, _width;
  bool _isArabic = false;
  bool _isEnglish = false;
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppLocalizations.of(context)!.translate('choose_lang'),
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              AppLocalizations.of(context)!.translate('select_lang'),
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            CustomContainer(
              width: _width! * 0.9,
              height: _height! * .066,
              hasHorizontalMargin: true,
              isSelected: _isArabic,
              child: InkWell(
                onTap: () {
                  setState(() {
                    if (!_isArabic) {
                      _isArabic = true;
                      _isEnglish = false;
                      Navigator.pushNamed(context, '/choose_country');
                    }
                  });
                },
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)!.translate('arabic'),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            CustomContainer(
              width: _width! * 0.9,
              height: _height! * .066,
              hasHorizontalMargin: true,
              isSelected: _isEnglish,
              child: InkWell(
                onTap: () {
                  setState(() {
                    if (!_isEnglish) {
                      _isArabic = false;
                      _isEnglish = true;
                    }
                  });
                },
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)!.translate('english'),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
