// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:matrix_client/custom_widgets/buttons/custom_button.dart';
// import 'package:matrix_client/custom_widgets/connectivity/network_indicator.dart';
// import 'package:matrix_client/custom_widgets/custom_appbar/custom_appbar.dart';
// import 'package:matrix_client/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
// import 'package:matrix_client/custom_widgets/custom_text_form_field/validation_mixin.dart';
// import 'package:matrix_client/custom_widgets/dialogs/verfiy_dialog.dart';
// import 'package:matrix_client/custom_widgets/safe_area/page_container.dart';
// import 'package:matrix_client/networking/api_provider.dart';
// import 'package:matrix_client/providers/auth_provider.dart';
// import 'package:matrix_client/utils/app_colors.dart';
// import 'package:matrix_client/utils/commons.dart';
// import 'package:matrix_client/utils/urls.dart';
// import 'package:provider/provider.dart';

// class ActivationCodeScreen extends StatefulWidget {
//   const ActivationCodeScreen({Key key}) : super(key: key);

//   @override
//   _ActivationCodeScreenState createState() => _ActivationCodeScreenState();
// }

// class _ActivationCodeScreenState extends State<ActivationCodeScreen>
//     with ValidationMixin {
//   double _height = 0, _width = 0;
//   String _activeCode = '';
//   final _formKey = GlobalKey<FormState>();
//   bool _isLoading = false;
//   ApiProvider _apiProvider = ApiProvider();
//   AuthProvider _authProvider;

//   Widget _buildBody() {
//     return SingleChildScrollView(
//       child: Form(
//           key: _formKey,
//           child: Column(
//             children: [
//               Center(
//                 child: Container(
//                     margin: EdgeInsets.only(top: _height * 0.25),
//                     child: Image.asset(
//                       'assets/images/logo.png',
//                       fit: BoxFit.cover,
//                     )),
//               ),
//               SizedBox(
//                 height: 30,
//               ),
//               Center(
//                   child: Text(
//                 "أدخل كود التفعيل المرسل على بريدك الإلكتروني",
//                 style: Theme.of(context).textTheme.headline3,
//                 textAlign: TextAlign.center,
//               )),
//               SizedBox(
//                 height: 10,
//               ),
//               Center(
//                   child: Text(
//                 _authProvider.userEmail,
//                 style:
//                     TextStyle(color: mainAppColor, fontWeight: FontWeight.w500),
//                 textAlign: TextAlign.center,
//               )),
//               SizedBox(
//                 height: 20,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 validationFunc: validateActivationCode,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/edite.png",
//                 hintTxt: "اكتب الكود ",
//                 onChangedFunc: (String text) {
//                   _activeCode = text;
//                   print('active code $_activeCode');
//                 },
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               _isLoading
//                   ? SpinKitFadingCircle(color: mainAppColor)
//                   : CustomButton(
//                       btnLbl: "تفعيل حسابك ",
//                       height: 50,
//                       onPressedFunction: () {
//                         _activeRegisterCode();
//                       }),
//               SizedBox(
//                 height: _height * 0.12,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Image.asset("assets/images/resend.png"),
//                   SizedBox(
//                     width: 10,
//                   ),
//                   Text(
//                     "لم يصلك الكود ؟ ",
//                     style: TextStyle(
//                         color: accentColor,
//                         fontWeight: FontWeight.w300,
//                         fontSize: 15),
//                     textAlign: TextAlign.center,
//                   ),
//                   GestureDetector(
//                     onTap: () => _resendActiveCode(),
//                     child: Text(
//                       " أعد الارسال",
//                       style: TextStyle(
//                           color: mainAppColor, fontWeight: FontWeight.w500),
//                       textAlign: TextAlign.center,
//                     ),
//                   )
//                 ],
//               )
//             ],
//           )),
//     );
//   }

//   _activeRegisterCode() async {
//     if (_formKey.currentState.validate()) {
//       setState(() {
//         _isLoading = true;
//       });
//       final result = await _apiProvider.post(Urls.ACTIVATION_CODE_URL, body: {
//         "code": _activeCode
//       }, header: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json',
//         'Accept-Language': 'ar',
//         'Authorization': 'Bearer ${_authProvider.activationToken}'
//         // _authProvider.currentLang
//       });
//       setState(() {
//         _isLoading = false;
//       });
//       print(result);
//       if (result['status_code'] == 200) {
//         _buildActivationAcountDialog();
//         Future.delayed(Duration(seconds: 3), () {
//           Navigator.pop(context);
//           Navigator.pushReplacementNamed(context, '/login_screen');
//         });
//       } else if (result['status_code'] == 422) {
//         if (result['response'].toString().contains('code'))
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['code'][0]);
//       }
//     }
//   }

//   Future _buildActivationAcountDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم تفعيل حسابك بنجاح',
//           );
//         });
//   }

//   _resendActiveCode() async {
//     final result =
//         await _apiProvider.post(Urls.RESEND_ACTIVE_CODE_URL, header: {
//       'Content-Type': 'application/json',
//       'Accept': 'application/json',
//       'Accept-Language': 'ar',
//       'Authorization': 'Bearer ${_authProvider.activationToken}'
//     });
//     if (result['status_code'] == 200) {
//       print(result);
//       _buildsendCodeDialog();
//       Future.delayed(Duration(seconds: 3), () {
//         Navigator.pop(context);
//       });
//     } else if (result['status_code'] == 422) {
//       print(result);
//       // if (result['response'].toString().contains('code'))
//       //   Commons.showError(context, result['response']['errors']['code'][0]);
//     }
//   }

//   Future _buildsendCodeDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم إرسال الكود على بريدك الإلكتروني ',
//             description: _authProvider.userEmail,
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     _height = MediaQuery.of(context).size.height;
//     _width = MediaQuery.of(context).size.width;
//     _authProvider = Provider.of<AuthProvider>(context);
//     return NetworkIndicator(
//       child: PageContainer(
//         child: Stack(
//           children: [
//             Image.asset(
//               "assets/images/bg.png",
//               height: _height,
//               width: _width,
//               fit: BoxFit.cover,
//             ),
//             Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CustomAppbar(
//                 hasBackBtn: true,
//                 hasDrawer: false,
//                 hasnotification: false,
//                 hasCenterLogo: false,
//                 title: 'تفعيل حسابك',
//               ),
//               body: _buildBody(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
