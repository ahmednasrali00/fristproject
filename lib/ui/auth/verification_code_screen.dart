import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// class VerificationCodeScreen extends StatefulWidget {
//   const VerificationCodeScreen({Key key}) : super(key: key);

//   @override
//   _VerificationCodeScreenState createState() => _VerificationCodeScreenState();
// }

// class _VerificationCodeScreenState extends State<VerificationCodeScreen>
//     with ValidationMixin {
//   double _height = 0, _width = 0;
//   final _formKey = GlobalKey<FormState>();
//   bool _isLoading = false;
//   String _code = '';
//   ApiProvider _apiProvider = ApiProvider();
//   AuthProvider _authProvider;

//   Widget _buildBody() {
//     return SingleChildScrollView(
//       child: Form(
//           key: _formKey,
//           child: Column(
//             children: [
//               Center(
//                 child: Container(
//                     margin: EdgeInsets.only(top: _height * 0.15),
//                     child: Image.asset(
//                       'assets/images/code.png',
//                       fit: BoxFit.cover,
//                       // height: _height,
//                       // width: _width,
//                     )),
//               ),
//               SizedBox(
//                 height: 30,
//               ),
//               Center(
//                   child: Text(
//                 "أدخل الكود المرسل على بريدك الإلكتروني",
//                 style: Theme.of(context).textTheme.headline3,
//                 textAlign: TextAlign.center,
//               )),
//               SizedBox(
//                 height: 10,
//               ),
//               Center(
//                   child: Text(
//                 _authProvider.userEmail,
//                 style:
//                     TextStyle(color: mainAppColor, fontWeight: FontWeight.w500),
//                 textAlign: TextAlign.center,
//               )),
//               SizedBox(
//                 height: 20,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 validationFunc: validateActivationCode,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/edite.png",
//                 hintTxt: "اكتب الكود ",
//                 onChangedFunc: (String text) {
//                   _code = text;
//                 },
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               _isLoading
//                   ? SpinKitFadingCircle(color: mainAppColor)
//                   : CustomButton(
//                       btnLbl: " استرجاع كلمة المرور",
//                       height: 50,
//                       onPressedFunction: () {
//                         confirmCode();
//                       }),
//               SizedBox(
//                 height: _height * 0.1,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Image.asset("assets/images/resend.png"),
//                   SizedBox(
//                     width: 10,
//                   ),
//                   Text(
//                     "لم يصلك الكود ؟ ",
//                     style: TextStyle(
//                         color: accentColor,
//                         fontWeight: FontWeight.w300,
//                         fontSize: 15),
//                     textAlign: TextAlign.center,
//                   ),
//                   GestureDetector(
//                     onTap: () => resendCode(),
//                     child: Text(
//                       " أعد الارسال",
//                       style: TextStyle(
//                           color: mainAppColor, fontWeight: FontWeight.w500),
//                       textAlign: TextAlign.center,
//                     ),
//                   )
//                 ],
//               )
//             ],
//           )),
//     );
//   }

//   confirmCode() async {
//     if (_formKey.currentState.validate()) {
//       setState(() {
//         _isLoading = true;
//       });
//       final result = await _apiProvider.post(Urls.CONFIRM_CODE_URL, body: {
//         'username': _authProvider.userEmail,
//         'code': _code
//       }, header: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json',
//         'Accept-Language': 'ar',
//       });
//       setState(() {
//         _isLoading = false;
//       });
//       print(result);
//       if (result['status_code'] == 200) {
//         print(result);
//         _authProvider.setRestToken(result['response']['reset_token']);
//         Navigator.pop(context);
//         Navigator.pushReplacementNamed(context, '/rest_password_screen');
//       } else if (result['status_code'] == 422) {
//         if (result['response'].toString().contains('username')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['username'][0]);
//         } else if (result['response'].toString().contains('code')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['code'][0]);
//         }
//       }
//     }
//   }

//   resendCode() async {
//     final result = await _apiProvider.post(Urls.FORGET_PASSWORD_URL, body: {
//       'username': _authProvider.userEmail
//     }, header: {
//       'Content-Type': 'application/json',
//       'Accept': 'application/json',
//       'Accept-Language': 'ar',
//     });

//     print(result);
//     if (result['status_code'] == 200) {
//       _buildVerificationCodeDialog();
//       Future.delayed(Duration(seconds: 3), () {
//         Navigator.pop(context);
//       });
//     } else if (result['status_code'] == 422) {
//       if (result['response'].toString().contains('username'))
//         Commons.showError(
//             context: context,
//             message: result['response']['errors']['username'][0]);
//     }
//   }

//   Future _buildVerificationCodeDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم إرسال الكود على بريدك الإلكتروني',
//             description: _authProvider.userEmail,
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     _height = MediaQuery.of(context).size.height;
//     _width = MediaQuery.of(context).size.width;
//     _authProvider = Provider.of<AuthProvider>(context);
//     return NetworkIndicator(
//       child: PageContainer(
//         child: Stack(
//           children: [
//             Image.asset(
//               "assets/images/bg.png",
//               height: _height,
//               width: _width,
//               fit: BoxFit.cover,
//             ),
//             Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CustomAppbar(
//                 hasBackBtn: true,
//                 hasDrawer: false,
//                 hasnotification: false,
//                 hasCenterLogo: true,
//               ),
//               body: _buildBody(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
