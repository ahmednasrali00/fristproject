// import 'package:flutter/material.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:matrix_client/custom_widgets/buttons/custom_button.dart';
// import 'package:matrix_client/custom_widgets/connectivity/network_indicator.dart';
// import 'package:matrix_client/custom_widgets/custom_appbar/custom_appbar.dart';
// import 'package:matrix_client/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
// import 'package:matrix_client/custom_widgets/custom_text_form_field/validation_mixin.dart';
// import 'package:matrix_client/custom_widgets/dialogs/verfiy_dialog.dart';
// import 'package:matrix_client/custom_widgets/safe_area/page_container.dart';
// import 'package:matrix_client/networking/api_provider.dart';
// import 'package:matrix_client/providers/auth_provider.dart';
// import 'package:matrix_client/utils/app_colors.dart';
// import 'package:matrix_client/utils/commons.dart';
// import 'package:matrix_client/utils/urls.dart';
// import 'package:provider/provider.dart';

// class ForgetPasswordScreen extends StatefulWidget {
//   const ForgetPasswordScreen({Key key}) : super(key: key);

//   @override
//   _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
// }

// class _ForgetPasswordScreenState extends State<ForgetPasswordScreen>
//     with ValidationMixin {
//   double _height = 0, _width = 0;
//   final _formKey = GlobalKey<FormState>();
//   bool _isLoading = false;
//   String _userEmail = '';
//   ApiProvider _apiProvider = ApiProvider();
//   AuthProvider _authProvider;

//   Future _buildVerificationCodeDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم إرسال الكود على بريدك الإلكتروني',
//             description: _userEmail,
//           );
//         });
//   }

//   Widget _buildBody() {
//     return SingleChildScrollView(
//         child: Form(
//             key: _formKey,
//             child: Container(
//               height: _height - 70,
//               width: _width,
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Center(
//                     child: Container(
//                         // width: _width ,
//                         //height: _height / 3,
//                         child: Image.asset(
//                       'assets/images/reset.png',
//                       fit: BoxFit.cover,
//                       // height: _height,
//                       // width: _width,
//                     )),
//                   ),
//                   SizedBox(
//                     height: 30,
//                   ),
//                   Center(
//                       child: Text(
//                     "أدخل البريد الإلكتروني لإرسال كود إسترجاع \nكلمة المرور",
//                     style: Theme.of(context).textTheme.headline3,
//                     textAlign: TextAlign.center,
//                   )),
//                   SizedBox(
//                     height: 20,
//                   ),
//                   CustomTextFormField(
//                     enableBorder: false,
//                     validationFunc: validateUserEmail,
//                     hasHorizontalMargin: true,
//                     prefixIconIsImage: true,
//                     prefixIconImagePath: "assets/images/mail.png",
//                     hintTxt: "البريد الإلكتروني ",
//                     onChangedFunc: (String text) {
//                       _userEmail = text;
//                     },
//                   ),
//                   SizedBox(
//                     height: 20,
//                   ),
//                   _isLoading
//                       ? SpinKitFadingCircle(color: mainAppColor)
//                       : CustomButton(
//                           btnLbl: "ارسال كود الاسترجاع",
//                           height: 50,
//                           onPressedFunction: () {
//                             _forgetPassword();
//                           }),
//                 ],
//               ),
//             )));
//   }

//   _forgetPassword() async {
//     if (_formKey.currentState.validate()) {
//       setState(() {
//         _isLoading = true;
//       });
//       final result = await _apiProvider.post(Urls.FORGET_PASSWORD_URL, body: {
//         'username': _userEmail
//       }, header: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json',
//         'Accept-Language': 'ar',
//       });
//       setState(() {
//         _isLoading = false;
//       });
//       print(result);
//       if (result['status_code'] == 200) {
//         _authProvider.setUserEmail(_userEmail);
//         _buildVerificationCodeDialog();
//         Future.delayed(Duration(seconds: 3), () {
//           Navigator.pop(context);
//           Navigator.pushReplacementNamed(context, '/verification_code_screen');
//         });
//       } else if (result['status_code'] == 422) {
//         if (result['response'].toString().contains('username'))
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['username'][0]);
//       }
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     _height = MediaQuery.of(context).size.height;
//     _width = MediaQuery.of(context).size.width;
//     _authProvider = Provider.of<AuthProvider>(context);
//     return NetworkIndicator(
//       child: PageContainer(
//         child: Stack(
//           children: [
//             Image.asset(
//               "assets/images/bg.png",
//               height: _height,
//               width: _width,
//               fit: BoxFit.cover,
//             ),
//             Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CustomAppbar(
//                 hasBackBtn: true,
//                 hasDrawer: false,
//                 hasnotification: false,
//                 hasCenterLogo: true,
//               ),
//               body: _buildBody(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
