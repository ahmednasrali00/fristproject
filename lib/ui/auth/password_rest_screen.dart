import 'package:flutter/material.dart';


// class PasswordRestScreen extends StatefulWidget {
//   const PasswordRestScreen({Key key}) : super(key: key);

//   @override
//   _PasswordRestScreenState createState() => _PasswordRestScreenState();
// }

// class _PasswordRestScreenState extends State<PasswordRestScreen>
//     with ValidationMixin {
//   double _height = 0, _width = 0;
//   String _password = '';
//   final _formKey = GlobalKey<FormState>();

//   bool _isLoading = false;
//   ApiProvider _apiProvider = ApiProvider();
//   AuthProvider _authProvider;

//   Widget _buildBody() {
//     return SingleChildScrollView(
//       child: Form(
//           key: _formKey,
//           child: Container(
//             height: _height,
//             child: Column(
//               children: [
//                 Center(
//                   child: Container(
//                       margin: EdgeInsets.only(top: _height * 0.1),
//                       child: Image.asset(
//                         'assets/images/lock.png',
//                         fit: BoxFit.cover,
//                         // height: _height,
//                         // width: _width,
//                       )),
//                 ),
//                 SizedBox(
//                   height: 30,
//                 ),
//                 Center(
//                     child: Text(
//                   "كلمة المرور الجديدة",
//                   style: Theme.of(context).textTheme.headline3,
//                   textAlign: TextAlign.center,
//                 )),
//                 SizedBox(
//                   height: 20,
//                 ),
//                 CustomTextFormField(
//                   enableBorder: false,
//                   isPassword: true,
//                   prefixIconIsImage: true,
//                   prefixIconImagePath: "assets/images/keyin.png",
//                   validationFunc: validatePassword,
//                   hintTxt: "كلمة المرور",
//                   onChangedFunc: (String text) {
//                     _password = text;
//                   },
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 CustomTextFormField(
//                     enableBorder: false,
//                     isPassword: true,
//                     prefixIconIsImage: true,
//                     prefixIconImagePath: "assets/images/keyin.png",
//                     hintTxt: " تأكيد كلمة المرور الجديدة",
//                     inputData: TextInputType.visiblePassword,
//                     validationFunc: validateConfirmPassword),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 _isLoading
//                     ? SpinKitFadingCircle(color: mainAppColor)
//                     : CustomButton(
//                         btnLbl: "تأكيد",
//                         height: 50,
//                         onPressedFunction: () {
//                           _restPassword();
//                         }),
//               ],
//             ),
//           )),
//     );
//   }

//   _restPassword() async {
//     if (_formKey.currentState.validate()) {
//       setState(() {
//         _isLoading = true;
//       });

//       final result = await _apiProvider.post(Urls.RESET_PASSWORD_URL, body: {
//         'token': _authProvider.restToken,
//         'password': _password,
//         'password_confirmation': _password
//       }, header: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json',
//         'Accept-Language': 'ar',
//       });
//       setState(() {
//         _isLoading = false;
//       });
//       if (result['status_code'] == 200) {
//         print(result);
//         _buildRestPasswordDialog();
//         Future.delayed(Duration(seconds: 3), () {
//           Navigator.pop(context);
//           Navigator.pushReplacementNamed(context, '/login_screen');
//         });
//       } else if (result['status_code'] == 422) {
//         if (result['response'].toString().contains('token')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['token'][0]);
//         } else if (result['response'].toString().contains('password')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['password'][0]);
//         } else if (result['response']
//             .toString()
//             .contains('password_confirmation')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['password_confirmation']
//                   [0]);
//         }
//       }
//     }
//   }

//   Future _buildRestPasswordDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم استرجاع كلمة المرور بنجاح',
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     _height = MediaQuery.of(context).size.height;
//     _width = MediaQuery.of(context).size.width;
//     _authProvider = Provider.of<AuthProvider>(context);
//     return NetworkIndicator(
//       child: PageContainer(
//         child: Stack(
//           children: [
//             Image.asset(
//               "assets/images/bg.png",
//               height: _height,
//               width: _width,
//               fit: BoxFit.cover,
//             ),
//             Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CustomAppbar(
//                 hasBackBtn: true,
//                 hasDrawer: false,
//                 hasnotification: false,
//                 hasCenterLogo: true,
//               ),
//               body: _buildBody(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
