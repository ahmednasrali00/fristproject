import 'package:flutter/material.dart';

// class UserRegisterScreen extends StatefulWidget {
//   const UserRegisterScreen({Key key}) : super(key: key);

//   @override
//   _UserRegisterScreenState createState() => _UserRegisterScreenState();
// }

// class _UserRegisterScreenState extends State<UserRegisterScreen>
//     with ValidationMixin {
//   double _height = 0, _width = 0;
//   String _userName = '', _userEmail = '', _userPhone = '', _userPassword = '';
//   bool _isLoading = false;
//   final _formKey = GlobalKey<FormState>();
//   ApiProvider _apiProvider = ApiProvider();
//   AuthProvider _authProvider;
//   Widget _buildBody() {
//     return SingleChildScrollView(
//       child: Container(
//         child: Form(
//           key: _formKey,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Container(
//                 alignment: Alignment.center,
//                 margin: EdgeInsets.only(top: _height * 0.04),
//                 child: Image.asset(
//                   'assets/images/logo.png',
//                   fit: BoxFit.cover,
//                 ),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/username.png",
//                 hintTxt: " اسمك",
//                 onChangedFunc: (String text) {
//                   _userName = text;
//                 },
//                 validationFunc: validateUserName,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/mail.png",
//                 inputData: TextInputType.emailAddress,
//                 hintTxt: "البريد الإلكتروني ",
//                 onChangedFunc: (String text) {
//                   _userEmail = text;
//                 },
//                 validationFunc: validateUserEmail,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/phonein.png",
//                 hintTxt: " رقم الجوال ",
//                 inputData: TextInputType.phone,
//                 onChangedFunc: (String text) {
//                   _userPhone = text;
//                 },
//                 validationFunc: validateUserPhone,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/keyin.png",
//                 hintTxt: " كلمة المرور",
//                 isPassword: true,
//                 onChangedFunc: (String text) {
//                   _userPassword = text;
//                 },
//                 validationFunc: validatePassword,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/keyin.png",
//                 hintTxt: "تأكيد كلمة المرور",
//                 isPassword: true,
//                 validationFunc: validateConfirmPassword,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               _isLoading
//                   ? SpinKitFadingCircle(color: mainAppColor)
//                   : CustomButton(
//                       btnLbl: "إنشاء حساب",
//                       onPressedFunction: () => _userRegister(),
//                     ),
//               SizedBox(
//                 height: 20,
//               ),
//               Center(
//                   child: Text(
//                 "لديك حساب بالفعل ؟",
//                 style: Theme.of(context).textTheme.headline3,
//               )),
//               SizedBox(
//                 height: 20,
//               ),
//               CustomButton(
//                 btnLbl: "تسجيل دخول",
//                 btnColor: Colors.white,
//                 borderColor: mainAppColor,
//                 btnStyle: TextStyle(
//                     color: mainAppColor,
//                     fontWeight: FontWeight.w400,
//                     fontSize: 16.0),
//                 onPressedFunction: () =>
//                     Navigator.pushNamed(context, '/login_screen'),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   _userRegister() async {
//     print('lang:${_authProvider.currentLang}');
//     if (_formKey.currentState.validate()) {
//       setState(() {
//         _isLoading = true;
//       });
//       final result = await _apiProvider.post(Urls.USER_REGISTER_URL, body: {
//         "name": _userName,
//         "email": _userEmail,
//         "phone": _userPhone,
//         "password": _userPassword,
//         "password_confirmation": _userPassword
//       }, header: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json',
//         'Accept-Language': 'ar'
//         // _authProvider.currentLang
//       });
//       setState(() {
//         _isLoading = false;
//       });
//       if (result['status_code'] == 201) {
//         print(result);
//         _authProvider.setActivationToken(result['response']['token']);
//         _authProvider.setUserEmail(result['response']['data']['email']);
//         _buildActivationCodeDialog();
//         Future.delayed(Duration(seconds: 3), () {
//           Navigator.pop(context);
//           Navigator.pushReplacementNamed(context, '/activation_code_screen');
//         });
//       } else if (result['status_code'] == 422) {
//         print(result);
//         if (result['response'].toString().contains('email'))
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['email'][0]);
//         else if (result['response'].toString().contains('phone')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['phone'][0]);
//         }
//       }
//     }
//   }

//   Future _buildActivationCodeDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم إرسال الكود على بريدك الإلكتروني ',
//             description: _userEmail,
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     _height = MediaQuery.of(context).size.height;
//     _width = MediaQuery.of(context).size.width;
//     _authProvider = Provider.of<AuthProvider>(context);

//     return NetworkIndicator(
//       child: PageContainer(
//         child: Stack(
//           children: [
//             Image.asset(
//               "assets/images/bg.png",
//               height: _height,
//               width: _width,
//               fit: BoxFit.cover,
//             ),
//             Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CustomAppbar(
//                 hasBackBtn: true,
//                 hasDrawer: false,
//                 hasnotification: false,
//                 hasCenterLogo: false,
//                 title: 'حساب جديد | مستخدم عادي',
//               ),
//               body: _buildBody(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
