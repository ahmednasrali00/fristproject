import 'dart:io';



// class OrganizationRegisterScreen extends StatefulWidget {
//   const OrganizationRegisterScreen({Key key}) : super(key: key);

//   @override
//   _OrganizationRegisterScreenState createState() =>
//       _OrganizationRegisterScreenState();
// }

// class _OrganizationRegisterScreenState extends State<OrganizationRegisterScreen>
//     with ValidationMixin {
//   double _height = 0, _width = 0;
//   String _organizatonName = '',
//       _responsePerson = '',
//       _noOfRegistrationCommercial = '',
//       _noOfTaxFile = '',
//       _organizationEmail = '',
//       _organizationPhone = '',
//       _organizationPassword = '';
//   bool _isLoading = false;
//   final _formKey = GlobalKey<FormState>();
//   final _picker = ImagePicker();
//   File _image;
//   String imageName = '';
//   bool hasImage = true;

//   ApiProvider _apiProvider = ApiProvider();
//   AuthProvider _authProvider;
//   Future getImage() async {
//     FocusScope.of(context).requestFocus(FocusNode());
//     final pickedFile = await _picker.getImage(source: ImageSource.gallery);
//     if (pickedFile != null) {
//       setState(() {
//         _image = File(pickedFile.path);
//         imageName = "\n${_image.path.split('/').last}";
//         hasImage = true;
//       });
//       print(imageName);
//     }
//   }

//   Widget _buildBody() {
//     return SingleChildScrollView(
//       child: Container(
//         child: Form(
//           key: _formKey,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               SizedBox(
//                 height: 10,
//               ),
//               DottedBorder(
//                 borderType: BorderType.RRect,
//                 radius: Radius.circular(15),
//                 dashPattern: [10, 7],
//                 //color: borderColor,
//                 color: Color(0xff8B1F1E1A).withOpacity(.2),
//                 strokeWidth: 1,
//                 child: Container(
//                   // margin: EdgeInsets.symmetric(horizontal: 2, vertical: 2),

//                   width: _width * 0.9,
//                   height: _height * 0.19,
//                   alignment: Alignment.center,
//                   decoration: BoxDecoration(
//                       color: grayColor,
//                       borderRadius: BorderRadius.all(Radius.circular(15))),
//                   child: Column(
//                     // mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       _image == null
//                           ? Container(
//                               margin: EdgeInsets.symmetric(vertical: 25),
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 crossAxisAlignment: CrossAxisAlignment.center,
//                                 children: [
//                                   Container(
//                                     margin: EdgeInsets.only(top: 5),
//                                     child: GestureDetector(
//                                       onTap: () {
//                                         print("add image");
//                                         getImage();
//                                       },
//                                       child: Image.asset(
//                                         'assets/images/addimage.png',
//                                       ),
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     width: 20,
//                                   ),
//                                   Container(
//                                     margin: EdgeInsets.only(top: 5),
//                                     width: 1.5,
//                                     height: 30,
//                                     color: Color(0xffEBEBEB),
//                                   ),
//                                   SizedBox(
//                                     width: 20,
//                                   ),
//                                   Container(
//                                     child: Column(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.center,
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.center,
//                                       children: [
//                                         Text(
//                                           'شعار الهيئة',
//                                           style: TextStyle(
//                                               color: Color(0xff404040),
//                                               fontWeight: FontWeight.w400,
//                                               fontSize: 15),
//                                         ),
//                                         SizedBox(
//                                           height: 5,
//                                         ),
//                                         Text(
//                                           'Png , Jpg , Jpeg',
//                                           textAlign: TextAlign.center,
//                                           style: TextStyle(
//                                               color: textGrayColor,
//                                               fontWeight: FontWeight.w400,
//                                               fontSize: 15),
//                                         )
//                                       ],
//                                     ),
//                                   )
//                                 ],
//                               ),
//                             )
//                           : Stack(
//                               children: [
//                                 Container(
//                                   alignment: Alignment.center,
//                                   margin: EdgeInsets.symmetric(
//                                       horizontal: _width * 0.01,
//                                       vertical: _height * 0.02),
//                                   child: CircleAvatar(
//                                       maxRadius: 40,
//                                       backgroundImage: FileImage(
//                                         File(_image.path),
//                                       )),
//                                 ),
//                                 Positioned(
//                                     top: 20,
//                                     right: 110,
//                                     child: Container(
//                                       width: 30,
//                                       height: 30,
//                                       child: FloatingActionButton(
//                                         backgroundColor: mainAppColor,
//                                         child: Icon(
//                                           Icons.close_rounded,
//                                           size: 20,
//                                         ),
//                                         onPressed: () {
//                                           setState(() {
//                                             _image = null;
//                                           });
//                                         },
//                                       ),
//                                     ))
//                               ],
//                             ),
//                       _image == null
//                           ? SizedBox(
//                               height: 5,
//                             )
//                           : Container(),
//                       Text(
//                         hasImage ? '' : ' من فضلك اختر شعار الهيئة',
//                         style: TextStyle(
//                             color: Colors.redAccent,
//                             fontSize: 15,
//                             fontWeight: FontWeight.w400),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/organizationin.png",
//                 hintTxt: "اسم الهيئة",
//                 onChangedFunc: (String text) {
//                   _organizatonName = text;
//                 },
//                 validationFunc: validateOrganizationName,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/respons.png",
//                 hintTxt: "اسم الشخص المسئول",
//                 onChangedFunc: (String text) {
//                   _responsePerson = text;
//                 },
//                 validationFunc: validateResponsibleName,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/editein.png",
//                 hintTxt: "رقم السجل التجاري",
//                 onChangedFunc: (String text) {
//                   _noOfRegistrationCommercial = text;
//                 },
//                 validationFunc: validateNoOfRegistrationCommercial,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/editein.png",
//                 hintTxt: "رقم الملف الضريبي",
//                 onChangedFunc: (String text) {
//                   _noOfTaxFile = text;
//                 },
//                 validationFunc: validateNoOfTaxFile,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/mail.png",
//                 hintTxt: "البريد الإلكتروني ",
//                 onChangedFunc: (String text) {
//                   _organizationEmail = text;
//                 },
//                 validationFunc: validateUserEmail,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/phonein.png",
//                 hintTxt: " رقم الجوال ",
//                 inputData: TextInputType.phone,
//                 onChangedFunc: (String text) {
//                   _organizationPhone = text;
//                 },
//                 validationFunc: validateUserPhone,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/keyin.png",
//                 hintTxt: " كلمة المرور",
//                 isPassword: true,
//                 onChangedFunc: (String text) {
//                   _organizationPassword = text;
//                 },
//                 validationFunc: validatePassword,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/keyin.png",
//                 hintTxt: "تأكيد كلمة المرور",
//                 isPassword: true,
//                 validationFunc: validateConfirmPassword,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               _isLoading
//                   ? SpinKitFadingCircle(color: mainAppColor)
//                   : CustomButton(
//                       btnLbl: "طلب انضمام",
//                       onPressedFunction: () => _organizationRegister(),
//                     ),
//               SizedBox(
//                 height: 20,
//               ),
//               Center(
//                   child: Text(
//                 "لديك حساب بالفعل ؟",
//                 style: Theme.of(context).textTheme.headline3,
//               )),
//               SizedBox(
//                 height: 20,
//               ),
//               CustomButton(
//                 btnLbl: "تسجيل دخول",
//                 btnColor: Colors.white,
//                 borderColor: mainAppColor,
//                 btnStyle: TextStyle(
//                     color: mainAppColor,
//                     fontWeight: FontWeight.w400,
//                     fontSize: 16.0),
//                 onPressedFunction: () =>
//                     Navigator.pushNamed(context, '/login_screen'),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   _organizationRegister() async {
//     if (!_formKey.currentState.validate()) {
//       return;
//     } else if (_image == null) {
//       setState(() {
//         hasImage = false;
//       });
//     } else {
//       _formKey.currentState.save();
//       setState(() {
//         _isLoading = true;
//       });

//       final header = {
//         //'Content-Type': 'application/json',
//         'Accept': 'application/json',
//         'Accept-Language': 'ar'
//       };
//       Map<String, dynamic> data = Map<String, dynamic>();
//       data['authority_name'] = _organizatonName;
//       data['name'] = _responsePerson;
//       data['commercial_number'] = _noOfRegistrationCommercial;
//       data['tax_file_number'] = _noOfTaxFile;
//       data['email'] = _organizationEmail;
//       data['phone'] = _organizationPhone;
//       data['password'] = _organizationPassword;
//       data['password_confirmation'] = _organizationPassword;
//       if (_image != null)
//         data['avatar'] = await MultipartFile.fromFile(_image.path);
//       FormData formData = FormData.fromMap(data);

//       final result = await _apiProvider.postWithDio(
//           Urls.ORGANIZATION_REGISTER_URL,
//           body: formData,
//           headers: header);
//       setState(() {
//         _isLoading = false;
//       });
//       print(result);
//       if (result['status_code'] == 201) {
//         _buildRqestJoiningDialog();
//         Future.delayed(Duration(seconds: 3), () {
//           Navigator.pop(context);
//           Navigator.pushReplacementNamed(context, '/login_screen');
//         });
//       } else if (result['status_code'] == 422) {
//         print(result);
//         if (result['response'].toString().contains('email'))
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['email'][0]);
//         else if (result['response'].toString().contains('phone')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['phone'][0]);
//         } else if (result['response']
//             .toString()
//             .contains('commercial_number')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['commercial_number'][0]);
//         } else if (result['response'].toString().contains('tax_file_number')) {
//           Commons.showError(
//               context: context,
//               message: result['response']['errors']['tax_file_number'][0]);
//         }
//       } else {
//         Commons.showError(
//             context: context, message: result['response']['message']);
//       }
//     }
//   }

//   Future _buildRqestJoiningDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم ارسال طلب الانضمام الى الادارة',
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     _height = MediaQuery.of(context).size.height;
//     _width = MediaQuery.of(context).size.width;
//     _authProvider = Provider.of<AuthProvider>(context);
//     return NetworkIndicator(
//       child: PageContainer(
//         child: Stack(
//           children: [
//             Image.asset(
//               "assets/images/bg.png",
//               height: _height,
//               width: _width,
//               fit: BoxFit.cover,
//             ),
//             Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CustomAppbar(
//                 hasBackBtn: true,
//                 hasDrawer: false,
//                 hasnotification: false,
//                 hasCenterLogo: false,
//                 title: 'حساب جديد | هيئة حكومية',
//               ),
//               body: _buildBody(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
