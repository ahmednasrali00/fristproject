

// class LoginScreen extends StatefulWidget {
//   const LoginScreen({Key key}) : super(key: key);

//   @override
//   _LoginScreenState createState() => _LoginScreenState();
// }

// class _LoginScreenState extends State<LoginScreen> with ValidationMixin {
//   double _height = 0, _width = 0;
//   String _userEmail = '';
//   String _userPassword = '';
//   bool _isLoading = false;
//   bool _rememberMe = false;
//   final _formKey = GlobalKey<FormState>();
//   ApiProvider _apiProvider = ApiProvider();
//   AuthProvider _authProvider;
//   FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
//   Widget _buildBody() {
//     return SingleChildScrollView(
//       child: Container(
//         // height: _height - 40,
//         child: Form(
//           key: _formKey,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Center(
//                 child: Container(
//                   child: Image.asset(
//                     'assets/images/logo.png',
//                     fit: BoxFit.cover,
//                   ),
//                 ),
//               ),
//               SizedBox(
//                 height: 30,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 hasHorizontalMargin: true,
//                 prefixIconIsImage: true,
//                 //inputData: TextInputType.emailAddress,
//                 prefixIconImagePath: "assets/images/mail.png",
//                 hintTxt: "البريد الإلكتروني ",
//                 onChangedFunc: (String text) {
//                   _userEmail = text;
//                 },
//                 validationFunc: validateUserEmail,
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               CustomTextFormField(
//                 enableBorder: false,
//                 isPassword: true,
//                 prefixIconIsImage: true,
//                 prefixIconImagePath: "assets/images/keyin.png",
//                 hintTxt: "كلمة المرور",
//                 onChangedFunc: (String text) {
//                   _userPassword = text;
//                 },
//                 validationFunc: validatePassword,
//               ),
//               SizedBox(
//                 height: 15,
//               ),
//               GestureDetector(
//                 onTap: () {
//                   setState(() {
//                     _rememberMe = !_rememberMe;
//                   });
//                   print(_rememberMe);
//                 },
//                 child: Container(
//                   padding: EdgeInsets.symmetric(horizontal: 2),
//                   margin: EdgeInsets.symmetric(horizontal: _width * 0.07),
//                   height: 30,
//                   child: Row(
//                     children: [
//                       Row(
//                         children: [
//                           Icon(
//                             _rememberMe
//                                 ? Icons.check_circle_rounded
//                                 : Icons.lens_outlined,
//                             color: mainAppColor,
//                           ),
//                           SizedBox(
//                             width: 5,
//                           ),
//                           Text(
//                             "تذكرني",
//                             style: Theme.of(context).textTheme.headline3,
//                           ),
//                           SizedBox(
//                             width: 5,
//                           ),
//                         ],
//                       ),
//                       Spacer(),
//                       GestureDetector(
//                         onTap: () {
//                           Navigator.pushNamed(
//                               context, '/forget_password_screen');
//                         },
//                         child: Text(
//                           "نسيت كلمة المرور ؟",
//                           style: Theme.of(context).textTheme.headline3,
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               _isLoading
//                   ? SpinKitFadingCircle(color: mainAppColor)
//                   : CustomButton(
//                       btnLbl: "دخول",
//                       height: 50,
//                       onPressedFunction: () {
//                         _userLogin();
//                       },
//                     ),
//               SizedBox(
//                 height: 10,
//               ),
//               Center(
//                   child: Text(
//                 "ليس لديك حساب ؟",
//                 style: Theme.of(context).textTheme.headline3,
//               )),
//               SizedBox(
//                 height: 10,
//               ),
//               CustomButton(
//                 btnLbl: "انشاء حساب جديد",
//                 btnColor: Colors.white,
//                 borderColor: mainAppColor,
//                 btnStyle: TextStyle(
//                     color: mainAppColor,
//                     fontWeight: FontWeight.w400,
//                     fontSize: 16.0),
//                 onPressedFunction: () =>
//                     Navigator.pushNamed(context, '/register_type_screen'),
//               ),
//               SizedBox(
//                 height: 30,
//               ),
//               // CustomButton(
//               //     btnLbl: "ارسال طلب صيانة سريع",
//               //     btnColor: Color(0xFF5959A1),
//               //     //  borderColor: mainAppColor,
//               //     btnStyle: TextStyle(
//               //         color: whiteColor,
//               //         fontWeight: FontWeight.w400,
//               //         fontSize: 16.0),
//               //     onPressedFunction: () {
//               //       _authProvider.currentUser != null
//               //           ? Navigator.pushNamed(
//               //               context, '/maintenance_request1_screen')
//               //           : Commons.showError(
//               //               context: context,
//               //               message: 'يجب عليك تسجيل الدخول اولا ');
//               //     }
//               //     // Navigator.pushNamed(context, '/register_type_screen'),
//               //     ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   _userLogin() async {
//     _firebaseMessaging.getToken().then((token) async {
//       print('firebase token: $token');

//       if (_formKey.currentState.validate()) {
//         setState(() {
//           _isLoading = true;
//         });

//         final result = await _apiProvider.post(Urls.LOGIN_URL, body: {
//           "username": _userEmail,
//           "password": _userPassword,
//           "firebase_id": token
//         }, header: {
//           'Content-Type': 'application/json',
//           'Accept': 'application/json',
//           'Accept-Language': _authProvider.currentLang
//         });
//         setState(() {
//           _isLoading = false;
//         });
//         print(result);
//         if (result['status_code'] == 200) {
//           print(result['response']['token']);
//           _authProvider
//               .setCurrentUser(User.fromJson(result['response']['data']));
//           _authProvider.setUserToken(result['response']['token']);
//           print('token${_authProvider.userToken}');
//           if (_rememberMe)
//             SharedPreferencesHelper.save('user', _authProvider.currentUser);
//           SharedPreferencesHelper.setUserToken(_authProvider.userToken);
//           // _buildSuccessLoginDialog();
//           Commons.showToast(message: 'تم تسجيل الدخول بنجاح', context: context);
//           Future.delayed(Duration(seconds: 3), () {
//             // Navigator.pop(context);
//             Navigator.pushReplacementNamed(context, '/home_screen');
//           });
//         } else if (result['status_code'] == 403) {
//           _authProvider.setUserEmail(_userEmail);
//           print(_authProvider.activationToken);
//           Commons.showError(
//               context: context,
//               message: result['response']['message'],
//               onTapFunk: () => Navigator.pushReplacementNamed(
//                   context, '/activation_code_screen'));
//           // Future.delayed(Duration(seconds: 3), () {
//           //   // Navigator.pop(context);
//           //   Navigator.pushReplacementNamed(context, '/activation_code_screen');
//           // });
//         } else if (result['status_code'] == 402) {
//           Commons.showError(
//               context: context, message: result['response']['message']);
//         } else if (result['status_code'] == 422) {
//           if (result['response'].toString().contains('username'))
//             Commons.showError(
//                 context: context,
//                 message: result['response']['errors']['username'][0]);
//         }
//       }
//     });
//   }

//   Future _buildSuccessLoginDialog() {
//     return showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return VerificationDialog(
//             verfiyImage: 'assets/images/done.png',
//             title: 'تم التسجيل بنجاح',
//           );
//         });
//   }

//   @override
//   Widget build(BuildContext context) {
//     _height = MediaQuery.of(context).size.height;
//     _width = MediaQuery.of(context).size.width;
//     _authProvider = Provider.of<AuthProvider>(context);
//     return NetworkIndicator(
//       child: PageContainer(
//         child: Stack(
//           children: [
//             Image.asset(
//               "assets/images/bg.png",
//               height: _height,
//               width: _width,
//               fit: BoxFit.cover,
//             ),
//             Scaffold(
//               backgroundColor: Colors.transparent,
//               appBar: CustomAppbar(
//                 title: "تسجيل الدخول",
//                 hasBackBtn: false,
//                 hasDrawer: false,
//                 hasnotification: false,
//                 hasCenterLogo: false,
//               ),
//               body: _buildBody(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
