import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sheraa/custom_widgets/custom_container/custom_container.dart';
import 'package:sheraa/custom_widgets/safe_area/page_container.dart';
import 'package:sheraa/locale/app_localizations.dart';
import 'package:sheraa/providers/auth_provider.dart';
import 'package:sheraa/utils/app_colors.dart';

class ChooseCountryScreen extends StatefulWidget {
  const ChooseCountryScreen({Key? key}) : super(key: key);

  @override
  _ChooseCountryScreenState createState() => _ChooseCountryScreenState();
}

class _ChooseCountryScreenState extends State<ChooseCountryScreen> {
  double? _height, _width;
  bool _isKSA = false;
  bool _isUAE = false;
  bool _isKuait = false;
  AuthProvider? _authProvider;

  Widget buildBodyContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Image.asset('assets/images/map.png'),
        Text(
          AppLocalizations.of(context)!.translate(
            "choose_country",
          ),
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        CustomContainer(
          isSelected: _isKSA,
          width: _width! * 0.1,
          height: _height! * .066,
          hasHorizontalMargin: true,
          child: InkWell(
            onTap: () {
              setState(() {
                if (!_isKSA) {
                  _isKSA = true;
                  _isUAE = false;
                  _isKuait = false;
                }
              });
              _authProvider!.setCurrentCountry('ksa');
              Navigator.pushNamed(context, '/select_gender');
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 15,
                ),
                Image.asset('assets/images/saudi-arabia.png'),
                SizedBox(
                  width: 10,
                ),
                Text(AppLocalizations.of(context)!.translate('sa'))
              ],
            ),
          ),
        ),
        CustomContainer(
          width: _width! * 0.1,
          height: _height! * .066,
          isSelected: _isUAE,
          child: InkWell(
            onTap: () {
              setState(() {
                if (!_isUAE) {
                  _isKSA = false;
                  _isUAE = true;
                  _isKuait = false;
                }
              });
              _authProvider!.setCurrentCountry('uae');
              Navigator.pushNamed(context, '/select_gender');
            },
            child: Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Image.asset('assets/images/united-arab-emirates.png'),
                SizedBox(
                  width: 15,
                ),
                Text(AppLocalizations.of(context)!.translate('uae'))
              ],
            ),
          ),
        ),
        CustomContainer(
          width: _width! * 0.1,
          height: _height! * .066,
          isSelected: _isKuait,
          child: InkWell(
            onTap: () {
              setState(() {
                if (!_isKuait) {
                  _isKSA = false;
                  _isUAE = false;
                  _isKuait = true;
                }
              });
              _authProvider!.setCurrentCountry('kuawit');
              Navigator.pushNamed(context, '/select_gender');
            },
            child: Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Image.asset('assets/images/kuwait.png'),
                SizedBox(
                  width: 15,
                ),
                Text(AppLocalizations.of(context)!.translate('kuwait'))
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _authProvider = Provider.of<AuthProvider>(context);
    return PageContainer(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: whiteColor,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: buildBodyContent(),
      ),
    );
  }
}
