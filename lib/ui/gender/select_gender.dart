import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sheraa/custom_widgets/buttons/custom_button.dart';
import 'package:sheraa/custom_widgets/custom_container/custom_container.dart';
import 'package:sheraa/custom_widgets/safe_area/page_container.dart';
import 'package:sheraa/locale/app_localizations.dart';
import 'package:sheraa/providers/auth_provider.dart';
import 'package:sheraa/utils/app_colors.dart';

class SelectGenderScreen extends StatefulWidget {
  const SelectGenderScreen({Key? key}) : super(key: key);

  @override
  _SelectGenderScreenState createState() => _SelectGenderScreenState();
}

class _SelectGenderScreenState extends State<SelectGenderScreen> {
  double _width = 0, _height = 0;
  bool _isMale = false,
      _isFemale = false,
      _isless18 = false,
      _isbetween18_25 = false,
      _isbetween25_30 = false,
      _isbetween30_35 = false,
      _ismore35 = false;
  AuthProvider? _authProvider;

  Widget bodyContent() {
    return ListView(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: _width * 0.5,
                  height: _height * 0.17,
                  child: CustomContainer(
                    hasHorizontalMargin: true,
                    isSelected: _isMale,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (!_isMale) {
                            _isMale = true;
                            _isFemale = false;
                          }
                        });
                        _authProvider!.setGender('male');
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/images/MAN.png'),
                          Text(AppLocalizations.of(context)!.translate('male'))
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  width: _width * 0.5,
                  height: _height * 0.17,
                  child: CustomContainer(
                    hasHorizontalMargin: true,
                    isSelected: _isFemale,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (!_isFemale) {
                            _isMale = false;
                            _isFemale = true;
                          }
                        });
                        _authProvider!.setGender('female');
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/images/women.png'),
                          Text(
                              AppLocalizations.of(context)!.translate('female'))
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              AppLocalizations.of(context)!.translate('age_category'),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            SizedBox(
              height: 20,
            ),
            CustomContainer(
              width: _width * 0.8,
              height: _height * 0.07,
              hasHorizontalMargin: true,
              isSelected: _isless18,
              child: InkWell(
                onTap: () {
                  setState(() {
                    if (!_isless18) {
                      _isless18 = true;
                      _isbetween18_25 = false;
                      _isbetween25_30 = false;
                      _isbetween30_35 = false;
                      _ismore35 = false;
                    }
                  });
                  _authProvider!.setAgeCategory('less18');
                },
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/checkbox.png',
                      color: _isless18 ? mainAppColor : grayColor,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(AppLocalizations.of(context)!.translate('less_18'))
                  ],
                ),
              ),
            ),
            CustomContainer(
              width: _width * 0.8,
              height: _height * 0.07,
              hasHorizontalMargin: true,
              isSelected: _isbetween18_25,
              child: InkWell(
                onTap: () {
                  setState(() {
                    if (!_isbetween18_25) {
                      _isless18 = false;
                      _isbetween18_25 = true;
                      _isbetween25_30 = false;
                      _isbetween30_35 = false;
                      _ismore35 = false;
                    }
                  });
                  _authProvider!.setAgeCategory('between18_25');
                },
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/checkbox.png',
                      color: _isbetween18_25 ? mainAppColor : grayColor,
                    ),
                    Text(
                        AppLocalizations.of(context)!.translate('between18_25'))
                  ],
                ),
              ),
            ),
            CustomContainer(
              width: _width * 0.8,
              height: _height * 0.07,
              hasHorizontalMargin: true,
              isSelected: _isbetween25_30,
              child: InkWell(
                onTap: () {
                  setState(() {
                    if (!_isbetween25_30) {
                      _isless18 = false;
                      _isbetween18_25 = false;
                      _isbetween25_30 = true;
                      _isbetween30_35 = false;
                      _ismore35 = false;
                    }
                  });
                  _authProvider!.setAgeCategory('between25_30');
                },
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/checkbox.png',
                      color: _isbetween25_30 ? mainAppColor : grayColor,
                    ),
                    Text(
                        AppLocalizations.of(context)!.translate('between25_30'))
                  ],
                ),
              ),
            ),
            CustomContainer(
              width: _width * 0.8,
              height: _height * 0.07,
              hasHorizontalMargin: true,
              isSelected: _isbetween30_35,
              child: InkWell(
                onTap: () {
                  setState(() {
                    if (!_isbetween30_35) {
                      _isless18 = false;
                      _isbetween18_25 = false;
                      _isbetween25_30 = false;
                      _isbetween30_35 = true;
                      _ismore35 = false;
                    }
                  });
                  _authProvider!.setAgeCategory('between30_35');
                },
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/checkbox.png',
                      color: _isbetween30_35 ? mainAppColor : grayColor,
                    ),
                    Text(
                        AppLocalizations.of(context)!.translate('between30_35'))
                  ],
                ),
              ),
            ),
            CustomContainer(
              width: _width * 0.8,
              height: _height * 0.07,
              hasHorizontalMargin: true,
              isSelected: _ismore35,
              child: InkWell(
                onTap: () {
                  setState(() {
                    if (!_ismore35) {
                      _isless18 = false;
                      _isbetween18_25 = false;
                      _isbetween25_30 = false;
                      _isbetween30_35 = false;
                      _ismore35 = true;
                    }
                  });
                  _authProvider!.setAgeCategory('more35');
                },
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/checkbox.png',
                      color: _ismore35 ? mainAppColor : grayColor,
                    ),
                    Text(AppLocalizations.of(context)!.translate('over35'))
                  ],
                ),
              ),
            ),
            CustomButton(
              btnLbl: AppLocalizations.of(context)!.translate(
                "continue",
              ),
              enableHorizontalMargin: true,
              enableVerticalMargin: true,
              enableCircleBorder: true,
              onPressedFunction: () {},
              btnColor: mainAppColor,
              btnStyle: TextStyle(color: whiteColor),
              borderColor: mainAppColor,
              height: _height * 0.067,
              width: _width * 0.8,
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _authProvider = Provider.of<AuthProvider>(context);

    return PageContainer(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: whiteColor,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.pop(context),
          ),
          title: Text(AppLocalizations.of(context)!.translate('choose_gender')),
          actions: [
            Container(
              padding: EdgeInsets.all(10),
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, '/bottom_navigation');
                },
                child: Text(AppLocalizations.of(context)!.translate('skip')),
              ),
            )
          ],
        ),
        body: bodyContent(),
      ),
    );
  }
}
