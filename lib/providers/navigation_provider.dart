import 'package:flutter/material.dart';
import 'package:sheraa/ui/ad/adding_ad_screen.dart';
import 'package:sheraa/ui/home/home_screen.dart';
import 'package:sheraa/ui/messages/messages_screen.dart';
import 'package:sheraa/ui/more/more_screen.dart';
import 'package:sheraa/ui/notifications/notifications_screen.dart';
// import 'package:haraj/ui/ad/adding_ad_screen.dart';
// import 'package:haraj/ui/chat/chat_screen.dart';
// import 'package:haraj/ui/favorites/favorites_screen.dart';
// import 'package:haraj/ui/home/home_screen.dart';
// import 'package:haraj/ui/more/more_screen.dart';

class NavigationProvider extends ChangeNotifier {
  int _navigationIndex = 0;

  void upadateNavigationIndex(int value) {
    _navigationIndex = value;
    notifyListeners();
  }

  int get navigationIndex => _navigationIndex;

  List<Widget> _screens = [
    HomeScreen(),
    NotificationsScreen(),
    AddingAdScreen(),
    MessagesScreen(),
    MoreScreen()
  ];

  Widget get selectedContent => _screens[_navigationIndex];
}
