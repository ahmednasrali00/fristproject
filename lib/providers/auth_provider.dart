import 'package:flutter/cupertino.dart';

class AuthProvider extends ChangeNotifier {
  // current language from shared prefernces 'ar' or 'en'
  String? _currentLang;

  void setCurrentLanguage(String currentLang) {
    _currentLang = currentLang;
    notifyListeners();
  }

  String get currentLang => _currentLang!;

  String? _country;

  void setCurrentCountry(String currentCountry) {
    _country = currentCountry;
    notifyListeners();
  }

  String get currentCountry => _country!;

  String? _gender;
  void setGender(String gender) {
    _gender = gender;
    notifyListeners();
  }

  String get gender => _gender!;

  String? _ageCategory;

  void setAgeCategory(String ageCategory) {
    _ageCategory = ageCategory;
    notifyListeners();
  }

  String get ageCategory => _ageCategory!;

  //AuthUser _currentUser;

  // void setCurrentUser(AuthUser currentUser) {
  //   _currentUser = currentUser;
  //   notifyListeners();
  // }

  //AuthUser get currentUser => _currentUser;

  String? _activationEmail;

  void setActivationEmail(String email) {
    _activationEmail = email;
    notifyListeners();
  }

  String get activationEmail => _activationEmail!;

  int? _userId;

  void setUserId(int id) {
    _userId = id;
    notifyListeners();
  }

  int get userId => _userId!;

  bool _verificationCodeError = false;

  void setVerificationCodeError(bool value) {
    _verificationCodeError = value;
    notifyListeners();
  }

  bool get verificationCodeError => _verificationCodeError;
}
