import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Color btnColor;
  final String btnLbl;
  final bool enableHorizontalMargin;
  final bool enableVerticalMargin;
  final bool enableCircleBorder;
  final double? height;
  final double? width;
  final Function onPressedFunction;
  final TextStyle btnStyle;

  final Color borderColor;

  const CustomButton(
      {required this.btnLbl,
      required this.onPressedFunction,
      required this.btnColor,
      required this.btnStyle,
      required this.borderColor,
      this.enableHorizontalMargin = true,
      this.enableVerticalMargin = true,
      this.enableCircleBorder = false,
      this.height,
      this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height == null ? 50 : height,
        width: width == null ? 300 : width,
        margin: EdgeInsets.symmetric(
            horizontal: enableHorizontalMargin
                ? MediaQuery.of(context).size.width * 0.05
                : 0.0,
            vertical: enableVerticalMargin
                ? MediaQuery.of(context).size.height * 0.01
                : 0.0),
        child: Builder(
            builder: (context) => RaisedButton(
                  onPressed: () {
                    onPressedFunction();
                  },
                  elevation: 0,
                  shape: new RoundedRectangleBorder(
                      side: BorderSide(
                          color: borderColor != null
                              ? borderColor
                              : btnColor != null
                                  ? btnColor
                                  : Theme.of(context).primaryColor),
                      borderRadius: new BorderRadius.circular(
                          enableCircleBorder ? 25 : 10.0)),
                  color: btnColor != null
                      ? btnColor
                      : Theme.of(context).primaryColor,
                  child: Container(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '$btnLbl',
                            style: btnStyle == null
                                ? Theme.of(context).textTheme.button
                                : btnStyle,
                          ),
                        ],
                      )),
                )));
  }
}
