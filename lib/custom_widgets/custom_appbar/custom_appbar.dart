// import 'package:badges/badges.dart';
// import 'package:flutter/material.dart';

// import 'package:provider/provider.dart';
// import 'package:sheraa/utils/app_colors.dart';

// class CustomAppbar extends StatelessWidget implements PreferredSizeWidget {
//   final bool hasBackBtn;
//   final String title;
//   final Widget leadingWidget;
//   final Widget trailingWidget;
//   final Function backButtonAction;
//   final bool hasDrawer;
//   final bool hasnotification;
//   final GlobalKey<ScaffoldState> scaffoldKey;
//   final bool hasCenterLogo;
//   @override
//   final Size preferredSize;

//   const CustomAppbar(
//       {this.hasDrawer: false,
//       required this.hasCenterLogo,
//       this.hasnotification: false,
//       required this.scaffoldKey,
//       this.hasBackBtn: false,
//       required this.leadingWidget,
//       required this.title,
//       required this.backButtonAction,
//       this.preferredSize: const Size.fromHeight(60.0),
//       required this.trailingWidget});

//   @override
//   Widget build(BuildContext context) {
//     // NotificationsProvider _notificationsProvider;
//     // _notificationsProvider = Provider.of<NotificationsProvider>(context);
//     // _notificationsProvider.getNotificationList(context);
//     // _notificationsProvider.getUnreadNotificationCount();
//     // print('count${_notificationsProvider.notificationCount}');
//     return Container(
//       width: MediaQuery.of(context).size.width,
//       //color: mainAppColor,
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           leadingWidget != null
//               ? leadingWidget
//               : hasBackBtn
//                   ? IconButton(
//                       icon: Icon(
//                         Icons.arrow_back,
//                         color: mainAppColor,
//                         size: 25,
//                       ),
//                       onPressed: backButtonAction == null
//                           ? () => Navigator.pop(context)
//                           : backButtonAction)
//                   : Container(),
//           Spacer(
//             flex: 3,
//           ),
//           hasCenterLogo
//               ? Container(
//                   child: Image.asset(
//                   'assets/images/logo.png',
//                   height: 40,
//                   width: 100,
//                 ))
//               : Text(title, style: Theme.of(context).textTheme.headline1),
//           Spacer(
//             flex: 3,
//           ),
//           hasnotification
//               ? Consumer<NotificationsProvider>(
//                   builder: (ctx, notifiProvider, _) => IconButton(
//                       icon: Badge(
//                           position:
//                               BadgePosition.topStart(start: -12, top: -12),
//                           toAnimate: false,
//                           badgeColor: Colors.black,
//                           badgeContent: Text(
//                             notifiProvider.notificationCount != 0
//                                 ? notifiProvider.notificationCount.toString()
//                                 : '0',
//                             // "0",
//                             style: TextStyle(color: Colors.white, fontSize: 10),
//                           ),
//                           child: Image.asset(
//                             'assets/images/notifi.png',
//                           )),
//                       onPressed: () {
//                         Navigator.pushReplacement(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (ctx) => NotificationsScreen()));
//                       }),
//                 )
//               : trailingWidget != null
//                   ? trailingWidget
//                   : Container(
//                       width: 20,
//                     ),
//         ],
//       ),
//     );
//   }
// }
