import 'package:flutter/material.dart';
import 'package:sheraa/utils/app_colors.dart';

class CustomContainer extends StatelessWidget {
  final Widget? child;
  final bool? isSelected;
  final double? width;
  final double? height;
  final bool hasHorizontalMargin;

  const CustomContainer(
      {Key? key,
      this.child,
      this.isSelected,
      this.width,
      this.height,
      this.hasHorizontalMargin = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
        width: width,
        height: height,
        padding: const EdgeInsets.all(8.0),
        margin: EdgeInsets.symmetric(
            horizontal: hasHorizontalMargin ? constraints.maxWidth * 0.08 : 0,
            vertical: constraints.maxWidth * 0.01),
        child: child,
        decoration: BoxDecoration(
            color: accentColor,
            borderRadius: BorderRadius.all(Radius.circular(18)),
            border: Border.all(
                color: isSelected == true ? mainAppColor : accentColor)),
      );
    });
  }
}
