import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:provider/provider.dart';
import 'package:sheraa/providers/auth_provider.dart';
import 'package:sheraa/providers/navigation_provider.dart';
import 'package:sheraa/shared_preferences/shared_preferences_helper.dart';
import 'package:sheraa/theme/style.dart';
import 'package:sheraa/utils/routes.dart';

import 'locale/app_localizations.dart';
import 'locale/locale_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) {
    run();
  });
}

void run() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Locale _locale;

  onLocaleChange(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  Future<void> _getLanguage() async {
    String language = await SharedPreferencesHelper.getUserLang();
    print('lan:$language');
    onLocaleChange(Locale(language));
  }

  @override
  void initState() {
    super.initState();
    helper.onLocaleChanged = onLocaleChange;
    _locale = new Locale('en');
    _getLanguage();
  }

  @override
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => NavigationProvider(),
        ),
        // ChangeNotifierProxyProvider<AuthProvider, RegisterProvider>(
        //   create: (_) => RegisterProvider(),
        //   update: (_, auth, registerProvider) =>
        //   registerProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, TrainersProvider>(
        //   create: (_) => TrainersProvider(),
        //   update: (_, auth, trainersProvider) =>
        //   trainersProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, NotificationProvider>(
        //   create: (_) => NotificationProvider(),
        //   update: (_, auth, notificationProvider) =>
        //   notificationProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, TrainerDetailsProvider>(
        //   create: (_) => TrainerDetailsProvider(),
        //   update: (_, auth, trainerDetailsProvider) =>
        //   trainerDetailsProvider..update(auth),
        // ),
        // ChangeNotifierProvider(
        //   create: (_) => CoursesProvider(),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, GalleryProvider>(
        //   create: (_) => GalleryProvider(),
        //   update: (_, auth, galleryProvider) =>
        //   galleryProvider..update(auth),
        // ),

        // ChangeNotifierProxyProvider<AuthProvider, StaticPagesProvider>(
        //   create: (_) => StaticPagesProvider(),
        //   update: (_, auth, staticPagesProvider) =>
        //   staticPagesProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, OffersProvider>(
        //   create: (_) => OffersProvider(),
        //   update: (_, auth, offerProvider) =>
        //   offerProvider..update(auth),
        // ),
        //   ChangeNotifierProxyProvider<AuthProvider, PackagesProvider>(
        //   create: (_) => PackagesProvider(),
        //   update: (_, auth, packageProvider) =>
        //   packageProvider..update(auth),
        // ),
        //  ChangeNotifierProxyProvider<AuthProvider, PackageDetailsProvider>(
        //   create: (_) => PackageDetailsProvider(),
        //   update: (_, auth, packageDetailsProvider) =>
        //   packageDetailsProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, BlogsProvider>(
        //   create: (_) => BlogsProvider(),
        //   update: (_, auth, blogsProvider) =>
        //   blogsProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, BlogDetailsProvider>(
        //   create: (_) => BlogDetailsProvider(),
        //   update: (_, auth, blogDetailsProvider) =>
        //   blogDetailsProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, CoursesProvider>(
        //   create: (_) => CoursesProvider(),
        //   update: (_, auth, coursesProvider) =>
        //   coursesProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, SubscribeDataProvider>(
        //   create: (_) => SubscribeDataProvider(),
        //   update: (_, auth, subscribeDataProvider) =>
        //   subscribeDataProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, UserProfileProvider>(
        //   create: (_) => UserProfileProvider(),
        //   update: (_, auth, userProfileProvider) =>
        //   userProfileProvider..update(auth),
        // ),

        // ChangeNotifierProxyProvider<AuthProvider, HomeProvider>(
        //   create: (_) => HomeProvider(),
        //   update: (_, auth, homeProvider) =>
        //   homeProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, SingleCourseProvider>(
        //   create: (_) => SingleCourseProvider(),
        //   update: (_, auth, singleCourseProvider) =>
        //   singleCourseProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, LessonProvider>(
        //   create: (_) => LessonProvider(),
        //   update: (_, auth, lessonProvider) =>
        //   lessonProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, SingleCategoryCoursesProvider>(
        //   create: (_) => SingleCategoryCoursesProvider(),
        //   update: (_, auth, singleCategoryCoursesProvider) =>
        //   singleCategoryCoursesProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, SearchProvider>(
        //   create: (_) => SearchProvider(),
        //   update: (_, auth, searchProvider) =>
        //   searchProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, ExamProvider>(
        //   create: (_) => ExamProvider(),
        //   update: (_, auth, examProvider) =>
        //   examProvider..update(auth),
        // ),
        // ChangeNotifierProxyProvider<AuthProvider, SocialProvider>(
        //   create: (_) => SocialProvider(),
        //   update: (_, auth, socialProvider) =>
        //   socialProvider..update(auth),
        // ),
      ],
      child: MaterialApp(
        locale: _locale,
        supportedLocales: [
          Locale('en', 'US'),
          Locale('ar', ''),
        ],
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        title: 'Sheraa',
        debugShowCheckedModeBanner: false,
        theme: themeData(),
        routes: routes,
      ),
    );
  }
}
